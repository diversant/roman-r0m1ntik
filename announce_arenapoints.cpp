﻿/*
#Patch: Announce Arena Points
#By: r0m1ntik
#Tested: Yes
#Work: Yes
#Update: 21.11.2016
*/
#include "ScriptPCH.h"

struct BroadcastData { uint32 time; };
 
struct BroadcastData Broadcasts[] =
{
    {1200000}, // 20 минут (в мили секундах) 
};
 
#define BROADCAST_COUNT  sizeof Broadcasts/sizeof(*Broadcasts)

class AnnounceArenaPoints : public WorldScript
{
public:
    AnnounceArenaPoints(): WorldScript("AnnounceArenaPoints") { }
	
	void OnStartup()
    {
		for(uint32 i = 0; i < BROADCAST_COUNT; i++)
        events.ScheduleEvent(i+1, Broadcasts[i].time);
    }

    void OnUpdate(uint32 diff)
    {
		events.Update(diff);
        while (uint32 id = events.ExecuteEvent())
        {
        if (id <= BROADCAST_COUNT)
		{
			QueryResult result1 = CharacterDatabase.PQuery("SELECT value FROM worldstates WHERE entry = 20001");
            if (result1)
            {
            Field * fields = NULL;
            fields = result1->Fetch();
            uint32 value = fields[0].GetUInt32();
			
			time_t autoDisTime = value;
            double waitTime = difftime(autoDisTime, time(NULL));
			
			int32 dWait = int(int(waitTime / 60) / 60) / 24;
            waitTime   -= dWait * 60 * 60 * 24;
            int32 hWait = int(waitTime / 60) / 60;
            waitTime   -= hWait * 60 * 60;
            int32 mWait = waitTime / 60;
            waitTime   -= mWait * 60;
            int32 sWait = waitTime;
			
		std::ostringstream ss;	
		if (dWait > 0)
		{
        ss << "|cffffffff Начесление [|cffffb3ff Очков арены |cffffffff] через: |cffffb3ff" << dWait << "|cffffffff дней |cffffb3ff" << hWait << "|cffffffff часов |cffffb3ff" << mWait << "|cffffffff минут |cffffb3ff";			
		}
        if (hWait > 0 && dWait == 0)
		{
        ss << "|cffffffff Начесление [|cffffb3ff Очков арены |cffffffff] через: |cffffb3ff" << hWait << "|cffffffff часов |cffffb3ff" << mWait << "|cffffffff минут |cffffb3ff";			
		}
        else
		{
        ss << "|cffffffff Начесление [|cffffb3ff Очков арены |cffffffff] через: |cffffb3ff" << mWait << "|cffffffff минут |cffffb3ff";			
		}			
        sWorld->SendServerMessage(SERVER_MSG_STRING, ss.str().c_str());
		events.ScheduleEvent(id, Broadcasts[id-1].time);
		
		}
        }    
        }
    }
private:
    EventMap events;
};

void AddSC_AnnounceArenaPoints()
{
    new AnnounceArenaPoints();
}