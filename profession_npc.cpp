﻿/*
#Profession PATCH update 3.3.5 -> cata
#By: r0m1ntik
#Date: 21.09.2016
*/

#include "Language.h"
     
    class Professions_NPC : public CreatureScript
    {
            public:
                    Professions_NPC () : CreatureScript("Professions_NPC") {}
                   
                    bool OnGossipHello(Player *pPlayer, Creature* _creature)
                    {
                            pPlayer->ADD_GOSSIP_ITEM(9, "|TInterface\\icons\\INV_Ingot_05:25:25:-15:0|tПрофессии", GOSSIP_SENDER_MAIN, 196);
							pPlayer->ADD_GOSSIP_ITEM(9, "|TInterface\\icons\\spell_holy_sealofsacrifice:25:25:-15:0|tВторичные навыки", GOSSIP_SENDER_MAIN, 197);
							//pPlayer->ADD_GOSSIP_ITEM(9, "|TInterface\\icons\\inv_elemental_eternal_shadow:25:25:-15:0|tРеагенты", GOSSIP_SENDER_MAIN, 198);
                            pPlayer->PlayerTalkClass->SendGossipMenu(1, _creature->GetGUID());
                            return true;
                    }
                   
                    bool PlayerAlreadyHasTwoProfessions(const Player *pPlayer) const
                    {
                            uint32 skillCount = 0;
     
                            if (pPlayer->HasSkill(SKILL_MINING))
                                    skillCount++;
                            if (pPlayer->HasSkill(SKILL_SKINNING))
                                    skillCount++;
                            if (pPlayer->HasSkill(SKILL_HERBALISM))
                                    skillCount++;
                            if (pPlayer->HasSkill(SKILL_ARCHAEOLOGY)) // для каты (новая профа)
                                    skillCount++;								
     
                            if (skillCount >= 4)
                                    return true;
     
                            for (uint32 i = 1; i < sSkillLineStore.GetNumRows(); ++i)
                            {
                                    SkillLineEntry const *SkillInfo = sSkillLineStore.LookupEntry(i);
                                    if (!SkillInfo)
                                            continue;
     
                                    if (SkillInfo->categoryId == SKILL_CATEGORY_SECONDARY)
                                            continue;
     
                                    if ((SkillInfo->categoryId != SKILL_CATEGORY_PROFESSION) || !SkillInfo->canLink)
                                            continue;
     
                                    const uint32 skillID = SkillInfo->id;
                                    if (pPlayer->HasSkill(skillID))
                                            skillCount++;
     
                                    if (skillCount >= 4)
                                            return true;
                            }
                            return false;
                    }
     
                    bool LearnAllRecipesInProfession(Player *pPlayer, SkillType skill)
                    {
                            ChatHandler handler(pPlayer->GetSession());
                            char* skill_name;
     
                            SkillLineEntry const *SkillInfo = sSkillLineStore.LookupEntry(skill);
                                                    skill_name = SkillInfo->name[handler.GetSessionDbcLocale()];
     
                            if (!SkillInfo)
                            {
                            TC_LOG_ERROR("server.loading", "Profession NPC: received non-valid skill ID (LearnAllRecipesInProfession)");
                            }      
     
                            LearnSkillRecipesHelper(pPlayer, SkillInfo->id);
     
                            pPlayer->SetSkill(SkillInfo->id, pPlayer->GetSkillStep(SkillInfo->id), 525, 525); // (ката 525, лич 450)
                            handler.PSendSysMessage(LANG_COMMAND_LEARN_ALL_RECIPES, skill_name);
                   
                            return true;
                    }
           
                    void LearnSkillRecipesHelper(Player *player, uint32 skill_id)
                    {
                            uint32 classmask = player->getClassMask();
     
                            for (uint32 j = 0; j < sSkillLineAbilityStore.GetNumRows(); ++j)
                            {
                                    SkillLineAbilityEntry const *skillLine = sSkillLineAbilityStore.LookupEntry(j);
                                    if (!skillLine)
                                            continue;
     
                                    // wrong skill
                                    if (skillLine->skillId != skill_id)
                                            continue;
     
                                    // not high rank
                                    if (skillLine->forward_spellid)
                                            continue;
     
                                    // skip racial skills
                                    if (skillLine->racemask != 0)
                                            continue;
     
                                    // skip wrong class skills
                                    if (skillLine->classmask && (skillLine->classmask & classmask) == 0)
                                            continue;
     
                                    SpellInfo const * spellInfo = sSpellMgr->GetSpellInfo(skillLine->spellId);
                                    if (!spellInfo || !SpellMgr::IsSpellValid(spellInfo, player, false))
                                            continue;
                                   
                                    player->learnSpell(skillLine->spellId, false);
                            }
                    }
     
                    bool IsSecondarySkill(SkillType skill) const
                    {
                            return skill == SKILL_COOKING || skill == SKILL_FIRST_AID || skill == SKILL_FISHING || skill == SKILL_ARCHAEOLOGY;
                    }
     
                    void CompleteLearnProfession(Player *pPlayer, Creature *pCreature, SkillType skill)
                    {
                            if (PlayerAlreadyHasTwoProfessions(pPlayer) && !IsSecondarySkill(skill))
                                    pCreature->MonsterWhisper("Вы уже выучили 4 профессии!", pPlayer);
                            else
                            {
                                    if (!LearnAllRecipesInProfession(pPlayer, skill))
                                            pCreature->MonsterWhisper("Internal error occured!", pPlayer);
                            }
                    }
           
                    bool OnGossipSelect(Player* pPlayer, Creature* _creature, uint32 uiSender, uint32 uiAction)
                    {
                            pPlayer->PlayerTalkClass->ClearMenus();
           
                            if (uiSender == GOSSIP_SENDER_MAIN)
                            {
                   
                                    switch (uiAction)
                                    {
                                            case 196:
											if (PlayerAlreadyHasTwoProfessions(pPlayer))
											{
											ChatHandler(pPlayer->GetSession()).PSendSysMessage("Вы уже выучили 4 професии");
											pPlayer->PlayerTalkClass->SendCloseGossip();
											}
                                            else
											{												
                                                    pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_INTERACT_2, "|TInterface\\icons\\trade_alchemy:20:20:-15:0|tАлхимия ", GOSSIP_SENDER_MAIN, 1);
                                                    pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_INTERACT_2, "|TInterface\\icons\\INV_Ingot_05:20:20:-15:0|tКузнечное дело ", GOSSIP_SENDER_MAIN, 2);
                                                    pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_INTERACT_2, "|TInterface\\icons\\INV_Misc_LeatherScrap_02:20:20:-15:0|tКожевничество ", GOSSIP_SENDER_MAIN, 3);
                                                    pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_INTERACT_2, "|TInterface\\icons\\INV_Fabric_Felcloth_Ebon:20:20:-15:0|tПортняжное дело ", GOSSIP_SENDER_MAIN, 4);
                                                    pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_INTERACT_2, "|TInterface\\icons\\inv_misc_wrench_01:20:20:-15:0|tИнженерное дело ", GOSSIP_SENDER_MAIN, 5);
                                                    pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_INTERACT_2, "|TInterface\\icons\\trade_engraving:20:20:-15:0|tНаложение чар ", GOSSIP_SENDER_MAIN, 6);
                                                    pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_INTERACT_2, "|TInterface\\icons\\inv_misc_gem_01:20:20:-15:0|tЮвелирное дело ", GOSSIP_SENDER_MAIN, 7);
                                                    pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_INTERACT_2, "|TInterface\\icons\\INV_Scroll_08:20:20:-15:0|tНачертание ", GOSSIP_SENDER_MAIN, 8);
                                                    pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_INTERACT_2, "|TInterface\\icons\\INV_Misc_Herb_07:20:20:-15:0|tТравничество ", GOSSIP_SENDER_MAIN, 9);
                                                    pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_INTERACT_2, "|TInterface\\icons\\inv_misc_pelt_wolf_01:20:20:-15:0|tСнятие шкур ", GOSSIP_SENDER_MAIN, 10);
                                                    pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_INTERACT_2, "|TInterface\\icons\\trade_mining:20:20:-15:0|tГорное дело ", GOSSIP_SENDER_MAIN, 11);
                                                    pPlayer->PlayerTalkClass->SendGossipMenu(1000005, _creature->GetGUID());
											}		
                                            break;
											
											case 197:
											    pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_INTERACT_2, "Список Професий которые вы можете обучить:", GOSSIP_SENDER_MAIN, 0);
											    if(!pPlayer->HasSkill(SKILL_FIRST_AID))
												{	
											    pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_INTERACT_2, "|TInterface\\icons\\spell_holy_sealofsacrifice:20:20:-15:0|tПервая Помощь ", GOSSIP_SENDER_MAIN, 13);
												}
												if(!pPlayer->HasSkill(SKILL_FISHING))
												{	
												pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_INTERACT_2, "|TInterface\\icons\\trade_fishing:20:20:-15:0|tРыбная ловля ", GOSSIP_SENDER_MAIN, 14);
												}
												if(!pPlayer->HasSkill(SKILL_COOKING))
												{	
												pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_INTERACT_2, "|TInterface\\icons\\inv_misc_food_15:20:20:-15:0|tКулинария ", GOSSIP_SENDER_MAIN, 15);
												}
												if(!pPlayer->HasSkill(SKILL_ARCHAEOLOGY))
												{	
												pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_INTERACT_2, "|TInterface\\icons\\trade_archaeology:20:20:-15:0|tАрхеология ", GOSSIP_SENDER_MAIN, 16);
												}												
                                                pPlayer->PlayerTalkClass->SendGossipMenu(1, _creature->GetGUID());
												if (pPlayer->HasSkill(SKILL_FISHING) && pPlayer->HasSkill(SKILL_FISHING) && pPlayer->HasSkill(SKILL_COOKING))
												{
												ChatHandler(pPlayer->GetSession()).PSendSysMessage("Вы уже выучили все професии");
											    pPlayer->PlayerTalkClass->SendCloseGossip();
												}	
												break;
												
									    /*	case 198: (нужен мультивендор патч)

                                            pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_INTERACT_2, "|TInterface\\icons\\inv_misc_gem_01:20:20:-15:0|tРеагенты Для Ювелира", GOSSIP_SENDER_MAIN, 30);
                                            pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_INTERACT_2, "|TInterface\\icons\\trade_mining:20:20:-15:0|tРеагенты Для Горного Дела", GOSSIP_SENDER_MAIN, 31);
                                            pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_INTERACT_2, "|TInterface\\icons\\trade_tailoring.:20:20:-15:0|tРеагенты Для Портного", GOSSIP_SENDER_MAIN, 32);
                                            pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_INTERACT_2, "|TInterface\\icons\\inv_inscription_tradeskill01:20:20:-15:0|tРеагенты Для Начертателя", GOSSIP_SENDER_MAIN, 33);
                                            pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_INTERACT_2, "|TInterface\\icons\\trade_engineering:20:20:-15:0|tРеагенты Для Инженера", GOSSIP_SENDER_MAIN, 34);
                                            pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_INTERACT_2, "|TInterface\\icons\\trade_engraving:20:20:-15:0|tРеагенты Для Наложения Чар", GOSSIP_SENDER_MAIN, 35);
                                            pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_INTERACT_2, "|TInterface\\icons\\inv_misc_armorkit_17:20:20:-15:0|tРеагенты Для Кожевника", GOSSIP_SENDER_MAIN, 36);
                                            pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_INTERACT_2, "|TInterface\\icons\\trade_alchemy:20:20:-15:0|tРеагенты Для Алхимика", GOSSIP_SENDER_MAIN, 37);
                                            pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_INTERACT_2, "|TInterface\\icons\\inv_elemental_eternal_shadow:20:20:-15:0|tРазные Товары", GOSSIP_SENDER_MAIN, 38);
                                            pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_INTERACT_2, "|TInterface\\icons\\inv_misc_herb_icethorn:20:20:-15:0|tТрава | Мясо | Рыба", GOSSIP_SENDER_MAIN, 39);
                                            pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_INTERACT_2, "|TInterface\\icons\\inv_hammer_20:20:20:-15:0|tУстройство | Приспособления", GOSSIP_SENDER_MAIN, 40);
                                            pPlayer->PlayerTalkClass->SendGossipMenu(1, _creature->GetGUID());	
                                            break;

                                            case 30:
											pPlayer->GetSession()->SendListInventory(_creature->GetGUID(), 560000);
											break;
                                            case 31:
											pPlayer->GetSession()->SendListInventory(_creature->GetGUID(), 551000);
											break;
                                            case 32:
											pPlayer->GetSession()->SendListInventory(_creature->GetGUID(), 558000);
											break;
                                            case 33:
											pPlayer->GetSession()->SendListInventory(_creature->GetGUID(), 557000);
											break;
                                            case 34:
											pPlayer->GetSession()->SendListInventory(_creature->GetGUID(), 555000);
											break;
                                            case 35:
											pPlayer->GetSession()->SendListInventory(_creature->GetGUID(), 556000);
											break;
                                            case 36:
											pPlayer->GetSession()->SendListInventory(_creature->GetGUID(), 552000);
											break;
                                            case 37:
											pPlayer->GetSession()->SendListInventory(_creature->GetGUID(), 550000);
											break;
                                            case 38:
											pPlayer->GetSession()->SendListInventory(_creature->GetGUID(), 553000);
											break;
                                            case 39:
											pPlayer->GetSession()->SendListInventory(_creature->GetGUID(), 559000);
											break;
                                            case 40:
											pPlayer->GetSession()->SendListInventory(_creature->GetGUID(), 554000);
                                            break;	*/																						
							
                                            case 1:
                                                    if(pPlayer->HasSkill(SKILL_ALCHEMY))
                                                    {
                                                            pPlayer->PlayerTalkClass->SendCloseGossip();
                                                            break;
                                                    }
     
                                                    CompleteLearnProfession(pPlayer, _creature, SKILL_ALCHEMY);
     
                                                    pPlayer->PlayerTalkClass->SendCloseGossip();
                                                    break;
                                            case 2:
                                                    if(pPlayer->HasSkill(SKILL_BLACKSMITHING))
                                                    {
                                                            pPlayer->PlayerTalkClass->SendCloseGossip();
                                                            break;
                                                    }
                                                    CompleteLearnProfession(pPlayer, _creature, SKILL_BLACKSMITHING);
     
                                                    pPlayer->PlayerTalkClass->SendCloseGossip();
                                                    break;
                                            case 3:
                                                    if(pPlayer->HasSkill(SKILL_LEATHERWORKING))
                                                    {
                                                            pPlayer->PlayerTalkClass->SendCloseGossip();
                                                            break;
                                                    }
                                                    CompleteLearnProfession(pPlayer, _creature, SKILL_LEATHERWORKING);
													pPlayer->learnSpell(57683, true);
													pPlayer->PlayerTalkClass->SendCloseGossip();
                                                    break;													

                                            case 4:
                                                    if(pPlayer->HasSkill(SKILL_TAILORING))
                                                    {
                                                            pPlayer->PlayerTalkClass->SendCloseGossip();
                                                            break;
                                                    }
                                                    CompleteLearnProfession(pPlayer, _creature, SKILL_TAILORING);
     
                                                    pPlayer->PlayerTalkClass->SendCloseGossip();
                                                    break;
                                            case 5:
												if (pPlayer->HasSkill(SKILL_ENGINEERING))
                                                    {
                                                            pPlayer->PlayerTalkClass->SendCloseGossip();
                                                            break;
                                                    }
												CompleteLearnProfession(pPlayer, _creature, SKILL_ENGINEERING);
     
                                                    pPlayer->PlayerTalkClass->SendCloseGossip();
                                                    break;
                                            case 6:
                                                    if(pPlayer->HasSkill(SKILL_ENCHANTING))
                                                    {
                                                            pPlayer->PlayerTalkClass->SendCloseGossip();
                                                            break;
                                                    }
                                                    CompleteLearnProfession(pPlayer, _creature, SKILL_ENCHANTING);
     
                                                    pPlayer->PlayerTalkClass->SendCloseGossip();
                                                    break;
                                            case 7:
                                                    if(pPlayer->HasSkill(SKILL_JEWELCRAFTING))
                                                    {
                                                            pPlayer->PlayerTalkClass->SendCloseGossip();
                                                            break;
                                                    }
                                                    CompleteLearnProfession(pPlayer, _creature, SKILL_JEWELCRAFTING);
     
                                                    pPlayer->PlayerTalkClass->SendCloseGossip();
                                                    break;
                                            case 8:
                                                    if(pPlayer->HasSkill(SKILL_INSCRIPTION))
                                                    {
                                                            pPlayer->PlayerTalkClass->SendCloseGossip();
                                                            break;
                                                    }
                                                    CompleteLearnProfession(pPlayer, _creature, SKILL_INSCRIPTION);
     
                                                    pPlayer->PlayerTalkClass->SendCloseGossip();
                                                    break;
                                            case 9:
                                                    if(pPlayer->HasSkill(SKILL_HERBALISM))
                                                    {
                                                        pPlayer->PlayerTalkClass->SendCloseGossip();
                                                        break;
                                                    }
                                                                                           
                                                    CompleteLearnProfession(pPlayer, _creature, SKILL_HERBALISM);
                                                    pPlayer->PlayerTalkClass->SendCloseGossip();
                                                    break;
                                            case 10:
                                                    if(pPlayer->HasSkill(SKILL_SKINNING))
                                                    {
                                                        pPlayer->PlayerTalkClass->SendCloseGossip();
                                                        break;
                                                    }
                                                                                           
                                                    CompleteLearnProfession(pPlayer, _creature, SKILL_SKINNING);
                                                    pPlayer->PlayerTalkClass->SendCloseGossip();
                                                    break;
                                            case 11:
                                                    if(pPlayer->HasSkill(SKILL_MINING))
                                                    {
                                                        pPlayer->PlayerTalkClass->SendCloseGossip();
                                                        break;
                                                    }
                                                                                           
                                                    CompleteLearnProfession(pPlayer, _creature, SKILL_MINING);
                                                    pPlayer->PlayerTalkClass->SendCloseGossip();
                                                    break;
																					case 13:
                                                                                            if(pPlayer->HasSkill(SKILL_FIRST_AID))
                                                                                            {
                                                                                                    pPlayer->PlayerTalkClass->SendCloseGossip();
                                                                                                    break;
                                                                                            }
                                                                                           
                                                                                            CompleteLearnProfession(pPlayer, _creature, SKILL_FIRST_AID);
                                                                                            pPlayer->PlayerTalkClass->SendCloseGossip();
                                                                                            break;			
																					case 14:
                                                                                            if(pPlayer->HasSkill(SKILL_FISHING))
                                                                                            {
                                                                                                    pPlayer->PlayerTalkClass->SendCloseGossip();
                                                                                                    break;
                                                                                            }
                                                                                           
                                                                                            CompleteLearnProfession(pPlayer, _creature, SKILL_FISHING);
                                                                                            pPlayer->PlayerTalkClass->SendCloseGossip();
                                                                                            break;
																					case 15:
                                                                                            if(pPlayer->HasSkill(SKILL_COOKING))
                                                                                            {
                                                                                                    pPlayer->PlayerTalkClass->SendCloseGossip();
                                                                                                    break;
                                                                                            }
                                                                                           
                                                                                            CompleteLearnProfession(pPlayer, _creature, SKILL_COOKING);
                                                                                            pPlayer->PlayerTalkClass->SendCloseGossip();
                                                                                            break;	

																					case 16:
                                                                                            if(pPlayer->HasSkill(SKILL_ARCHAEOLOGY))
                                                                                            {
                                                                                                    pPlayer->PlayerTalkClass->SendCloseGossip();
                                                                                                    break;
                                                                                            }
                                                                                           
                                                                                            CompleteLearnProfession(pPlayer, _creature, SKILL_ARCHAEOLOGY);
                                                                                            pPlayer->PlayerTalkClass->SendCloseGossip();
                                                                                            break;																							
                                    }
     
           
                            }
                            return true;
                    }
    };
     
    void AddSC_Professions_NPC()
    {
        new Professions_NPC();
    }