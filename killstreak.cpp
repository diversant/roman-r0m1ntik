/*
-- Execute this query in WORLD DATABASE
INSERT INTO creature_template
VALUES (189009, 0, 0, 0, 0, 0, 26571, 0, 0, 0, 'Gambling', '', 'Speak', 0, 80, 80, 2, 35, 3, 1, 1.14286, 1, 0, 0, 2000, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 7, 138936390, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0, 3, 1, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 'Gambler', 12340);

INSERT INTO npc_text
VALUES (999062, NULL, 'Gamble and you could double your coins. Good luck, $N.', 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 12340);

*/

#include "ScriptMgr.h"
#include "ScriptedCreature.h"
#include "ScriptedGossip.h"
#include "GameEventMgr.h"
#include "Player.h"
#include "WorldSession.h"
#include "Chat.h"

#define EfirID 38186 // ид эфириальной монеты
/*  тут настройка по количеству   [все уже настроено с автоматом рачетом приза в умножение на 2] */
#define CostEfir1 10 
#define CostEfir2 20
#define CostEfir3 50
#define CostEfir4 100
#define CostEfir5 250
#define CostEfir6 500

class Gamble_npc : public  CreatureScript
{
public:
	Gamble_npc() : CreatureScript("Gambler") {}

	bool OnGossipHello(Player* player, Creature* creature)
	{
		player->PlayerTalkClass->ClearMenus();
		if (player->HasItemCount(EfirID, CostEfir1))
			player->ADD_GOSSIP_ITEM(6, "|TInterface\\Icons\\INV_Ingot_01:25:25:-17:0|tИспытать свой шанс [ " + CostEfir1 " Эфириальных Монет ]", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 1);

		if (player->HasItemCount(EfirID, CostEfir2))
			player->ADD_GOSSIP_ITEM(6, "|TInterface\\Icons\\INV_Ingot_01:25:25:-17:0|tИспытать свой шанс [ " + CostEfir2 " Эфириальных Монет ]", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 2);

		if (player->HasItemCount(EfirID, CostEfir3))
			player->ADD_GOSSIP_ITEM(6, "|TInterface\\Icons\\INV_Ingot_01:25:25:-17:0|tИспытать свой шанс [ " + CostEfir3 " Эфириальных Монет ]", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 3);

		if (player->HasItemCount(EfirID, CostEfir4))
			player->ADD_GOSSIP_ITEM(6, "|TInterface\\Icons\\INV_Ingot_01:25:25:-17:0|tИспытать свой шанс [ " + CostEfir4 " Эфириальных Монет ]", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 4);

		if (player->HasItemCount(EfirID, CostEfir5))
			player->ADD_GOSSIP_ITEM(6, "|TInterface\\Icons\\INV_Ingot_01:25:25:-17:0|tИспытать свой шанс [ " + CostEfir5 " Эфириальных Монет ]", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 5);

		if (player->HasItemCount(EfirID, CostEfir6))
			player->ADD_GOSSIP_ITEM(6, "|TInterface\\Icons\\INV_Ingot_01:25:25:-17:0|tИспытать свой шанс [ " + CostEfir6 " Эфириальных Монет ]", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 6);

		else
		{
			player->ADD_GOSSIP_ITEM(6, "|TInterface\\Icons\\INV_Ingot_01:25:25:-17:0|t У вас не достаточно Эфириальных монет для игры", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 7);
			player->ADD_GOSSIP_ITEM(6, "|TInterface\\Icons\\INV_Ingot_01:25:25:-17:0|t Вам нужно как минимум [|cffff0000 " + CostEfir1 "|r Монет ].", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 7);
		}
		player->PlayerTalkClass->SendGossipMenu(999062, creature->GetGUID());
		return true;
	}

	bool OnGossipSelect(Player* player, Creature * creature, uint32 Sender, uint32 action)
	{
		uint32 number = urand(1, 3); // 33% шанса

		player->PlayerTalkClass->ClearMenus();
		if (Sender == GOSSIP_SENDER_MAIN)
		switch (action)
		{
			case GOSSIP_ACTION_INFO_DEF + 1:
            {			
				if (number == 3)
				{
					uint32 reward = CostEfir1 * 2;
					player->AddItem(EfirID, reward);
					ChatHandler(player->GetSession()).PSendSysMessage("|cff00ff00Поздравляем! Вы выйграли %u Эфириальных монет", reward);
					player->PlayerTalkClass->SendCloseGossip();
					return true;
				}
				else
				{
					player->DestroyItemCount(EfirID, CostEfir1, true);
					ChatHandler(player->GetSession()).PSendSysMessage("|cffff0000К сожелению вы проиграли %u Эфириальных монет.", ConstEfir1);
					player->PlayerTalkClass->SendCloseGossip();
					return true;
				}
			}
		    break;
			
			case GOSSIP_ACTION_INFO_DEF + 2:
            {			
				if (number == 3)
				{
					uint32 reward = CostEfir2 * 2;
					player->AddItem(EfirID, reward);
					ChatHandler(player->GetSession()).PSendSysMessage("|cff00ff00Поздравляем! Вы выйграли %u Эфириальных монет", reward);
					player->PlayerTalkClass->SendCloseGossip();
					return true;
				}
				else
				{
					player->DestroyItemCount(EfirID, CostEfir2, true);
					ChatHandler(player->GetSession()).PSendSysMessage("|cffff0000К сожелению вы проиграли %u Эфириальных монет.", ConstEfir2);
					player->PlayerTalkClass->SendCloseGossip();
					return true;
				}
			}
		    break;
			
			case GOSSIP_ACTION_INFO_DEF + 3:
            {			
				if (number == 3)
				{
					uint32 reward = CostEfir3 * 2;
					player->AddItem(EfirID, reward);
					ChatHandler(player->GetSession()).PSendSysMessage("|cff00ff00Поздравляем! Вы выйграли %u Эфириальных монет", reward);
					player->PlayerTalkClass->SendCloseGossip();
					return true;
				}
				else
				{
					player->DestroyItemCount(EfirID, CostEfir3, true);
					ChatHandler(player->GetSession()).PSendSysMessage("|cffff0000К сожелению вы проиграли %u Эфириальных монет.", ConstEfir3);
					player->PlayerTalkClass->SendCloseGossip();
					return true;
				}
			}
		    break;
			
			case GOSSIP_ACTION_INFO_DEF + 4:
            {			
				if (number == 3)
				{
					uint32 reward = CostEfir4 * 2;
					player->AddItem(EfirID, reward);
					ChatHandler(player->GetSession()).PSendSysMessage("|cff00ff00Поздравляем! Вы выйграли %u Эфириальных монет", reward);
					player->PlayerTalkClass->SendCloseGossip();
					return true;
				}
				else
				{
					player->DestroyItemCount(EfirID, CostEfir4, true);
					ChatHandler(player->GetSession()).PSendSysMessage("|cffff0000К сожелению вы проиграли %u Эфириальных монет.", ConstEfir4);
					player->PlayerTalkClass->SendCloseGossip();
					return true;
				}
			}
		    break;
			
			case GOSSIP_ACTION_INFO_DEF + 5:
            {			
				if (number == 3)
				{
					uint32 reward = CostEfir5 * 2;
					player->AddItem(EfirID, reward);
					ChatHandler(player->GetSession()).PSendSysMessage("|cff00ff00Поздравляем! Вы выйграли %u Эфириальных монет", reward);
					player->PlayerTalkClass->SendCloseGossip();
					return true;
				}
				else
				{
					player->DestroyItemCount(EfirID, CostEfir5, true);
					ChatHandler(player->GetSession()).PSendSysMessage("|cffff0000К сожелению вы проиграли %u Эфириальных монет.", ConstEfir5);
					player->PlayerTalkClass->SendCloseGossip();
					return true;
				}
			}
		    break;
			
			case GOSSIP_ACTION_INFO_DEF + 6:
            {			
				if (number == 3)
				{
					uint32 reward = CostEfir6 * 2;
					player->AddItem(EfirID, reward);
					ChatHandler(player->GetSession()).PSendSysMessage("|cff00ff00Поздравляем! Вы выйграли %u Эфириальных монет", reward);
					player->PlayerTalkClass->SendCloseGossip();
					return true;
				}
				else
				{
					player->DestroyItemCount(EfirID, CostEfir6, true);
					ChatHandler(player->GetSession()).PSendSysMessage("|cffff0000К сожелению вы проиграли %u Эфириальных монет.", ConstEfir6);
					player->PlayerTalkClass->SendCloseGossip();
					return true;
				}
			}
		    break;
			
            case GOSSIP_ACTION_INFO_DEF + 7:
            OnGossipHello(player, creature);
            break;			

		 }
		return true;
	}

};

void AddSC_Gamble_npc()
{
	new Gamble_npc;
}